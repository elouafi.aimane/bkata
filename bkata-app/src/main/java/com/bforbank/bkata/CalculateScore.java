package com.bforbank.bkata;

import com.bforbank.bkata.game.service.GameManager;
import com.bforbank.bkata.game.service.GameService;

import java.util.Scanner;

public class CalculateScore {
    private static GameManager gameManager = new GameService();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a game: ");

        String game = scanner.nextLine();
        gameManager.showScore(game);

    }
}
