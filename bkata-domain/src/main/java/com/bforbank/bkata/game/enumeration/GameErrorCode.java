package com.bforbank.bkata.game.enumeration;

public enum GameErrorCode {

    NULL_OR_EMPTY_GAME,
    INVALID_GAME_LENGTH,
    ONLY_PLAYER_A_AND_B_ARE_ACCEPTED,
    OUT_OF_RANGE_GAME,
    NOT_FINISHED_GAME;
}
