package com.bforbank.bkata.game.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Player {
    A('A'),
    B('B');

    private char value;
}
