package com.bforbank.bkata.game.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum Score {

    SCORE_0(0, "0"),
    SCORE_1(1, "15"),
    SCORE_2(2, "30"),
    SCORE_3(3, "40"),
    SCORE_4(4, "Advantage"),
    SCORE_5(5, "Winner");

    private int score;
    private String value;

    public static Score getScoreValueByScore(final int score){
        return Arrays.stream(Score.values())
                .filter(s -> score == s.score)
                .findFirst()
                .get();
    }
}
