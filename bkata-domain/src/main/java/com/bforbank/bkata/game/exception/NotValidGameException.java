package com.bforbank.bkata.game.exception;

import com.bforbank.bkata.game.enumeration.GameErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class NotValidGameException extends RuntimeException {
    private String message;
    private GameErrorCode gameErrorCode;
}
