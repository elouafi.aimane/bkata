package com.bforbank.bkata.game.service;

public interface GameManager {

    void addScore(String game);
    int[] getScore(String game);
    void showScore(String game);
}
