package com.bforbank.bkata.game.service;

import com.bforbank.bkata.game.enumeration.GameErrorCode;
import com.bforbank.bkata.game.enumeration.Player;
import com.bforbank.bkata.game.enumeration.Score;
import com.bforbank.bkata.game.exception.NotValidGameException;

import static java.lang.System.out;

public class GameService implements GameManager {

    private static final String SCORE_SENTENCE = "Player A : %s / Player B : %s\n";
    private static final String WINNER_SENTENCE = "Player %s wins the game\n";
    private static final String ADVANTAGE_SENTENCE = "Player %s has advantage\n";

    private static void printScore(final int scorePlayerA, final int scorePlayerB) {
        final Score scoreA = Score.getScoreValueByScore(scorePlayerA);
        final Score scoreB = Score.getScoreValueByScore(scorePlayerB);

        if (scoreA.getScore() == 4 && scoreB.getScore() == 3) {
            printAdvantage(Player.A);
        } else if (scoreA.getScore() == 3 && scoreB.getScore() == 4) {
            printAdvantage(Player.B);
        } else if (scoreA.getScore() >= 4) {
            printWinner(Player.A);
        } else if (scoreB.getScore() >= 4) {
            printWinner(Player.B);
        } else {
            out.printf(SCORE_SENTENCE, scoreA.getValue(), scoreB.getValue());
        }
    }

    private static void printWinner(final Player player) {
        out.printf(WINNER_SENTENCE, player.getValue());
    }

    private static void printAdvantage(final Player player) {
        out.printf(ADVANTAGE_SENTENCE, player.getValue());
    }

    private static void checkGame(final String game) {
        if (game == null || game.isEmpty()) {
            throw new NotValidGameException("game is null or empty, go play a game and come back", GameErrorCode.NULL_OR_EMPTY_GAME);
        }

        if (game.length() < 4) {
            throw new NotValidGameException("""
                    invalid game length, minimum required
                    game rounds to win is 4, the game is not complete yet.
                    Go finish the game and come back""", GameErrorCode.INVALID_GAME_LENGTH);
        }

    }

    private static void checkScore(final int scoreA, final int scoreB) {
        if (scoreA == 5 || scoreB == 5 || (scoreA == 4 && scoreB < 3) || (scoreB == 4 && scoreA < 3)) {
            throw new NotValidGameException("Winner already designed, the game can't continue", GameErrorCode.OUT_OF_RANGE_GAME);
        }
    }

    private static void checkIsFinishedGame(int scorePlayerA, int scorePlayerB) {
        if ((scorePlayerA < 4 && scorePlayerB < 4) || (scorePlayerA == 4 && scorePlayerB == 3) || (scorePlayerA == 3 && scorePlayerB == 4)) {
            throw new NotValidGameException("No one won the game, it needs to be finished", GameErrorCode.NOT_FINISHED_GAME);
        }
    }

    @Override
    public void addScore(String game) {

    }

    @Override
    public int[] getScore(String game) {
        return new int[0];
    }

    @Override
    public void showScore(final String game) {
        checkGame(game);
        char[] rounds = game.toCharArray();
        int scorePlayerA = 0;
        int scorePlayerB = 0;

        for (int i = 0; i < rounds.length; i++) {
            checkScore(scorePlayerA, scorePlayerB);

            if (rounds[i] == Player.A.getValue()) {
                scorePlayerA++;
            } else if (rounds[i] == Player.B.getValue()) {
                scorePlayerB++;
            } else {
                throw new NotValidGameException("not valid player character", GameErrorCode.ONLY_PLAYER_A_AND_B_ARE_ACCEPTED);
            }

            if (scorePlayerA == 4 && scorePlayerB == 4) {
                scorePlayerA--;
                scorePlayerB--;
            }

            printScore(scorePlayerA, scorePlayerB);
        }

        checkIsFinishedGame(scorePlayerA, scorePlayerB);
    }
}
