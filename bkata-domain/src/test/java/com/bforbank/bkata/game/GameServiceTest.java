package com.bforbank.bkata.game;

import com.bforbank.bkata.game.enumeration.GameErrorCode;
import com.bforbank.bkata.game.exception.NotValidGameException;
import com.bforbank.bkata.game.service.GameService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class GameServiceTest {

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @InjectMocks
    private GameService gameService;

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    @DisplayName("No one play a game")
    void testShowScoreEmptyGame() {
        //Given
        final String game = "";

        //When Then
        NotValidGameException exception = assertThrows(NotValidGameException.class, () -> gameService.showScore(game));
        assertEquals(exception.getGameErrorCode(), GameErrorCode.NULL_OR_EMPTY_GAME);
    }

    @Test
    @DisplayName("scenario ABA, Not complete game, no one win")
    void testShowScoreInvalidGameLength() {
        //Given
        final String game = "ABA";

        //When Then
        NotValidGameException exception = assertThrows(NotValidGameException.class, () -> gameService.showScore(game));
        assertEquals(exception.getGameErrorCode(), GameErrorCode.INVALID_GAME_LENGTH);
    }

    @Test
    @DisplayName("scenario ABCAA, More then 3 players are playing")
    void testShowScoreNotValidPlayer() {
        //Given
        final String game = "ABCAA";

        //When Then
        NotValidGameException exception = assertThrows(NotValidGameException.class, () -> gameService.showScore(game));
        assertEquals(exception.getGameErrorCode(), GameErrorCode.ONLY_PLAYER_A_AND_B_ARE_ACCEPTED);
    }

    @Test
    @DisplayName("scenario AAAAA, Game continue after A wins")
    void testShowScoreGameContinueAfterAWins() {
        //Given
        final String game = "AAAAA";

        //When Then
        NotValidGameException exception = assertThrows(NotValidGameException.class, () -> gameService.showScore(game));
        assertEquals(exception.getGameErrorCode(), GameErrorCode.OUT_OF_RANGE_GAME);
    }


    @Test
    @DisplayName("scenario ABBBBB, Game continue after B wins")
    void testShowScoreGameContinueAfterBWins() {
        //Given
        final String game = "ABBBBB";

        //When Then
        NotValidGameException exception = assertThrows(NotValidGameException.class, () -> gameService.showScore(game));
        assertEquals(exception.getGameErrorCode(), GameErrorCode.OUT_OF_RANGE_GAME);
    }

    @Test
    @DisplayName("scenario ABBAAA, A win a game")
    void testShowScoreGameValidAWins() {
        //Given
        final String game = "ABBAAA";

        //When
        gameService.showScore(game);

        //Then
        assertEquals("""
                Player A : 15 / Player B : 0
                Player A : 15 / Player B : 15
                Player A : 15 / Player B : 30
                Player A : 30 / Player B : 30
                Player A : 40 / Player B : 30
                Player A wins the game
                """, outputStreamCaptor.toString());
    }

    @Test
    @DisplayName("scenario ABBBAAAA, A win a game with one advatange")
    void testShowScoreGameValidAWinsWithAdvantage() {
        //Given
        final String game = "ABBBAAAA";

        //When
        gameService.showScore(game);

        //Then
        assertEquals("""
                Player A : 15 / Player B : 0
                Player A : 15 / Player B : 15
                Player A : 15 / Player B : 30
                Player A : 15 / Player B : 40
                Player A : 30 / Player B : 40
                Player A : 40 / Player B : 40
                Player A has advantage
                Player A wins the game
                """, outputStreamCaptor.toString());
    }

    @Test
    @DisplayName("scenario ABABABABBB, B win a game with one advantage")
    void testShowScoreGameValidBWinsWithAdvantage() {
        //Given
        final String game = "ABABABABBB";

        //When
        gameService.showScore(game);

        //Then
        assertEquals("""
                Player A : 15 / Player B : 0
                Player A : 15 / Player B : 15
                Player A : 30 / Player B : 15
                Player A : 30 / Player B : 30
                Player A : 40 / Player B : 30
                Player A : 40 / Player B : 40
                Player A has advantage
                Player A : 40 / Player B : 40
                Player B has advantage
                Player B wins the game
                """, outputStreamCaptor.toString());
    }

    @Test
    @DisplayName("scenario ABABABABA, not finished game")
    void testShowScoreGameNotFinishedGame() {
        //Given
        final String game = "ABABABABA";

        //When Then
        NotValidGameException exception = assertThrows(NotValidGameException.class, () -> gameService.showScore(game));
        assertEquals(exception.getGameErrorCode(), GameErrorCode.NOT_FINISHED_GAME);
    }

    @Test
    @DisplayName("scenario ABABA, not finished game")
    void testShowScoreGameNotFinishedSmallGame() {
        //Given
        final String game = "ABABA";

        //When Then
        NotValidGameException exception = assertThrows(NotValidGameException.class, () -> gameService.showScore(game));
        assertEquals(exception.getGameErrorCode(), GameErrorCode.NOT_FINISHED_GAME);
    }
}
